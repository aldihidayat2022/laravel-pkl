<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>Data Barang</legend>
        <table border="1">
            <tr>
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Nama Supplier</th>
            </tr>
            @foreach($barang as $data_barang)
                <tr>
                    <td>{{ $data_barang->nama_barang }}</td>
                    <td>{{ $data_barang->harga }}</td>
                    <td>{{ $data_barang->stok }}</td>
                    <td>{{ $data_barang->nama_supplier }}</td>
                </tr>
            @endforeach
        </table>
    </fieldset>
     <fieldset>
        <legend>Supplier</legend>
        <table border="1">
            <tr>
                <th>Nama Supplier</th>
                <th>No Telp</th>
                <th>Alamat</th>
            </tr>
            @foreach($supplier as $data_supplier)
                <tr>
                    <td>{{ $data_supplier->nama_supplier }}</td>
                    <td>{{ $data_supplier->no_telp }}</td>
                    <td>{{ $data_supplier->alamat }}</td>
                </tr>
            @endforeach
        </table>
    </fieldset>
     <fieldset>
        <legend>pembeli</legend>
        <table border="1">
            <tr>
                <th>Nama pembeli</th>
                <th>Jenis Kelamin</th>
                <th>No Telp</th>
                <th>Alamat</th>
            </tr>
            @foreach($pembeli as $data_pembeli)
                <tr>
                    <td>{{ $data_pembeli->nama_pembeli }}</td>
                    <td>{{ $data_pembeli->jk }}</td>
                    <td>{{ $data_pembeli->no_telp }}</td>
                     <td>{{ $data_pembeli->alamat }}</td>
                </tr>
            @endforeach
        </table>
    </fieldset>
     <fieldset>
        <legend>pembayaran</legend>
        <table border="1">
            <tr>
                <th>Tanggal Bayar</th>
                <th>Total Bayar</th>
                <th>Kode Transaksi</th>
            </tr>
            @foreach($pembayaran as $data_pembayaran)
                <tr>
                   
                    <td>{{ $data_pembayaran->tgl_bayar }}</td>
                    <td>{{ $data_pembayaran->total_bayar }}</td>
                     <td>{{ $data_pembayaran->kode_transaksi }}</td>
                </tr>
            @endforeach
        </table>
    </fieldset>
     <fieldset>
        <legend>Transaksi</legend>
        <table border="1">
            <tr>
                <th>Nama Barang</th>
                <th>Nama Pembeli</th>
                <th>Tanggal</th>
                <th>Keterangan</th>
            </tr>
            @foreach($transaksi as $data_transaksi)
                <tr>
                    <td>{{ $data_transaksi->nama_barang }}</td>
                    <td>{{ $data_transaksi->nama_pembeli }}</td>
                    <td>{{ $data_transaksi->tanggal }}</td>
                     <td>{{ $data_transaksi->keterangan }}</td>
                </tr>
            @endforeach
        </table>
    </fieldset>
</body>