<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DATA SISWA</title>
</head>
<body>
    <fieldset>
        <legend>
            Data Dosen
        </legend>
        @foreach($dosen as $dosen)
        @php
            $nilai_b = 0;
        @endphp
          Nama : {{ $dosen['name'] }} <br>
          Mata Kuliah : {{ $dosen['mata_kuliah']}}
        @foreach($dosen['mahasiswa'] as $mahasiswa)
        <li>Nama Mahasiswa : {{ $mahasiswa['nama'] }}</li>
        <li>Nilai : {{$mahasiswa ['nilai']}} </li>
        <?php $nilai_b += $mahasiswa ['nilai'] ?>
        @endforeach
        <hr>
        NILAI = {{ $nilai_b }} <br>
        RATA RATA = {{ $nilai_b / count($dosen['mahasiswa'])}} <br>
        <hr>
        @endforeach
    </fieldset>
</body>
</html>