<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TOKO</title>
</head>
<body>
    <fieldset>
        <legend>YUK BELANJA DI TOKO KAMI</legend>
        @foreach($toko as $a)
        <hr>
        ************************* <br>
        NAMA KONSUMEN TOKO  : {{$a['nama_orang']}} <br>
        ************************* 
        <?php $total = 0; ?>
        @foreach($a['pembelian'] as $kategori )
        <li> JENIS : {{ $kategori ['nama_barang'] }}  <br> </li>
        MERK : {{$kategori ['merk']}} <br>
        HARGA : {{ number_format($kategori['harga_barang'],0,",",".") }} <br>
        <hr>
        <?php $total += $kategori['harga_barang'] ?>
        HARGA : {{  number_format($total) ,0,",","."}} <br>
        @endforeach
            @if($total > 250000 && $total <500000)
            @php
                $cashback = (5/100) * $total @endphp 
             @elseif($total >=50000)
             @php
                $cashback = (10/100) * $total
             @endphp              
             @else
            @endif
            CASHBACK :  {{  number_format($cashback),0,",","."  }} <br>
            TOTAL HARGA : {{ number_format($total - $cashback,0,",",".")  }}
           
        @endforeach
    </fieldset>
</body>
</html>