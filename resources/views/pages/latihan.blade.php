<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PEMESANAN</title>
</head>
<body>
   <H1>saya memesan :</H1> 
   <br>
   Makanan : {{$makanan}} <br>
   Minuman : {{$minuman}} <br>
   Cemilan : {{$cemilan}} <br>
   Harga Makanan : 

   @if($makanan == "mie goreng")
   {{$harga_makanan = 5000}}
   @elseif($makanan == "seblak")
   {{$harga_makanan = 7500}}
   @elseif($makanan == "nasi padang")
   {{$harga_makanan = 15000}}
   @else
   {{$harga_makanan = "tidak ada input"}}
   @endif
<br>
Harga Minuman :

@if($minuman == "kopi")
{{$harga_minuman = 5000}}
@elseif($minuman == "es teh")
{{$harga_minuman = 7500}}
@elseif($minuman == "jus")
{{$harga_minuman = 15000}}
@else
{{$harga_minuman = "tidak ada input"}}
@endif

<br>

cemilan :
@if($cemilan == "kacang")
{{$harga_cemilan = 2000}}
@elseif($cemilan == "pilus")
{{$harga_cemilan = 500}}
@elseif($cemilan == "cikruh")
{{$harga_cemilan = 3000}}
@else
{{$harga_cemilan = "tidak ada input"}}
@endif

</body>
</html