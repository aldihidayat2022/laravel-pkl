<?php

use App\Http\Controllers\LatihanController;
use App\Http\Controllers\LierController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('about', function () {
    return view("tentang");
});
Route::get('profil', function () {
    $nama = "Aldi Hidayat";
});
Route::get('biodata', function () {
    $nama = "Aldi Hidayat";
    $umur = "17 tahun";
    $alamat = "situ tarate v";
    $jk = "laki - laki";
    $hobi = "bermain";
    return view('pages.biodata', compact('nama', 'umur', 'alamat', 'alamat', 'jk', 'hobi'));
});
Route::get('biodata1/{nama}', function ($a) {
    return view('pages.biodata1', compact('a'));
});
Route::get('order/{makanan}/{minuman}/{harga}', function ($d, $b, $c) {
    return view('pages.order', compact('d', 'b', 'c'));
});
Route::get('pesan/{menu?}', function ($a = "_") {
    return view('pages.pesan', compact('a'));
});
Route::get('latihan/{makanan?}/{minuman?}/{cemilan?}', function ($makanan = "SILAHKAN MASUKAN PESANAN ANDA", $minuman = "Silahkan masukan minuman anda", $cemilan = "Silahkan masukan cemilan anda") {
    return view('pages.latihan', compact('makanan', 'minuman', 'cemilan'));
});
Route::get('latihan1/{nama?}/{alamat}/{umur}', [LatihanController::class, 'perkenalan']);

Route::get('siswa/{id?}/{nama?}/{age?}', [LatihanController::class, 'siswa']);
Route::get('dosen', [LatihanController::class, 'dosen']);
Route::get('televisi', [LatihanController::class, 'televisi']);
Route::get('toko', [LatihanController::class, 'toko']);

// route post
Route::get('post', [PostController::class, 'tampil']);
Route::get('post/{id}', [PostController::class, 'search']);
Route::get('post/judul/{tittle}', [PostController::class, 'search_tittle']);
Route::get('post/edit/{id}/{tittle}/{content}', [PostController::class, 'edit']);
Route::get('post/tambah/{tittle}/{content}', [PostController::class, 'tambah']);
Route::get('post/delete/{id}', [PostController::class, 'hapus']);

// Penjualan
Route::get('/lier', [LierController::class, 'tampil']);
