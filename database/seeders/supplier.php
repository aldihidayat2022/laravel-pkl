<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class supplier extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supplier = [
            ['nama_supplier' => 'Rizki Fadhilah',
                'no_telp' => 866538787,
                'alamat' => 'TVI',
            ],
            ['nama_supplier' => 'dinan',
                'no_telp' => 8966554,
                'alamat' => 'cangkuang',
            ],
            ['nama_supplier' => 'azmil',
                'no_telp' => 8668765,
                'alamat' => 'cibaduyut',
            ],
            ['nama_supplier' => 'akbar',
                'no_telp' => 9876545678,
                'alamat' => 'sukamenak',
            ],
            ['nama_supplier' => 'aldi hidayat',
                'no_telp' => 831142925,
                'alamat' => 'padjajaran',
            ],
        ];
        DB::table('suppliers')->insert($supplier);
    }
}
