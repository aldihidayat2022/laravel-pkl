<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class barang extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $barang = [

            ['nama_barang' => 'Laptop',
                'harga' => 250000,
                'stok' => 100,
                'nama_supplier' => 'Agus Bancet',
            ],
            ['nama_barang' => 'infocus',
                'harga' => 50000,
                'stok' => 100,
                'nama_supplier' => 'sofyan',
            ],
            ['nama_barang' => 'usb',
                'harga' => 75000,
                'stok' => 100,
                'nama_supplier' => 'gilang bahrudin',
            ],
            ['nama_barang' => 'cpu',
                'harga' => 25000000,
                'stok' => 100,
                'nama_supplier' => ' salsabila azzahra',
            ],
            ['nama_barang' => 'keyboard',
                'harga' => 125000,
                'stok' => 100,
                'nama_supplier' => 'trisakoromo',
            ],

        ];
        DB::table('barangs')->insert($barang);

    }
}
