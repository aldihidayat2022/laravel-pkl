<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [
            ['tittle' => 'Belajar Huruf Hijaiyah',
                'content' => 'Lorem Ipsum sit amet dolor'],
            ['tittle' => 'Indonesia U-19 Gagal Lolos Semi Final',
                'content' => 'Lorem Ipsum sit amet dolor'],
            ['tittle' => 'Cara Cepat Belajar Pemograman',
                'content' => 'Try & Error Terus Menerus'],
        ];

        DB::table('posts')->insert($sample);
    }
}
