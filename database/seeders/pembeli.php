<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class pembeli extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pembeli = [
            ['nama_pembeli' => 'Aldi Hidayat',
                'jk' => 'Laki-Laki',
                'no_telp' => '83162925',
                'alamat' => 'jl.Terusan Cibaduyut',
            ],
            ['nama_pembeli' => 'ulya umajidah',
                'jk' => 'perempuan',
                'no_telp' => '83168654',
                'alamat' => 'cibogo',
            ],
            ['nama_pembeli' => 'adila',
                'jk' => 'perempuan',
                'no_telp' => '9876543344',
                'alamat' => 'cibaduyut',
            ],
            ['nama_pembeli' => 'azmil',
                'jk' => 'Laki-Laki',
                'no_telp' => '8316982925',
                'alamat' => 'jaksel',
            ],
            ['nama_pembeli' => 'dika',
                'jk' => 'Laki-Laki',
                'no_telp' => '83164442925',
                'alamat' => 'sumedang',
            ],
        ];
        DB::table('pembelis')->insert($pembeli);

    }

}
