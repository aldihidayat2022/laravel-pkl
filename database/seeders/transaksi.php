<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class transaksi extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transaksi = [
            ['nama_barang' => 'Komputer',
                'nama_pembeli' => 'Herdi',
                'tanggal' => '2022-09-09',
                'keterangan' => 'barang di beli karena bagus',

            ],
            ['nama_barang' => 'Hardisk',
                'nama_pembeli' => 'Maya',
                'tanggal' => '2022-10-02',
                'keterangan' => 'barang di beli karena bagus',

            ],
            ['nama_barang' => 'mouse',
                'nama_pembeli' => 'gibral',
                'tanggal' => '2022-06-20',
                'keterangan' => 'barang di beli karena bagus',

            ],
            ['nama_barang' => 'lcd',
                'nama_pembeli' => 'adzura',
                'tanggal' => '2022-12-02',
                'keterangan' => 'barang di beli karena bagus',

            ],
            ['nama_barang' => 'battre',
                'nama_pembeli' => 'dika',
                'tanggal' => '2022-10-01',
                'keterangan' => 'barang di beli karena bagus',

            ],
        ];
        DB::table('transaksis')->insert($transaksi);

    }
}
