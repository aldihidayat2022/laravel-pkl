<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class pembayaran extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pembayaran = [
            ['tgl_bayar' => '2022-08-11',
                'total_bayar' => 150000,
                'kode_transaksi' => 9987,
            ],
            ['tgl_bayar' => '2022-05-21',
                'total_bayar' => 250000,
                'kode_transaksi' => 8765,
            ],
            ['tgl_bayar' => '2022-12-21',
                'total_bayar' => 400000,
                'kode_transaksi' => 6789,
            ],
            ['tgl_bayar' => '2022-12-09',
                'total_bayar' => 100000,
                'kode_transaksi' => 8798,
            ],
            ['tgl_bayar' => '2022-07-02',
                'total_bayar' => 350000,
                'kode_transaksi' => 99875,
            ],
        ];
        DB::table('pembayarans')->insert($pembayaran);

    }
}
