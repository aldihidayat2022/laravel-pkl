<?php

namespace App\Http\Controllers;

use App\Models\barang;
use App\Models\pembayaran;
use App\Models\pembeli;
use App\Models\Supplier;
use App\Models\transaksi;

class LierController extends Controller
{
    public function tampil()
    {
        $barang = barang::all();
        $supplier = Supplier::all();
        $pembeli = pembeli::all();
        $pembayaran = pembayaran::all();
        $transaksi = transaksi::all();

        return view('post.lier', compact('barang', 'supplier', 'pembeli', 'pembayaran', 'transaksi'));
    }
}
