<?php

namespace App\Http\Controllers;

class LatihanController extends Controller
{
    public function perkenalan($nama, $alamat, $umur)
    {
        //passing variable to view
        $nama1 = $nama;
        $alamat1 = $alamat;
        $umur1 = $umur;

        return view('pages.perkenalan', compact('nama1', 'alamat1', 'umur1'));
    }

    public function siswa()
    {
        $a = [
            ['id' => 1, 'name' => 'Dadang', 'age' => 15, 'hobi' => ['MAKAN', 'TIDUR']],
            ['id' => 2, 'name' => 'Dudung', 'age' => 18, 'hobi' => ['TIDUR', 'MAKAN']],
        ];
        return view('pages.siswa', ['siswa' => $a]);
    }
    public function dosen()
    {
        $a = [
            ['id' => 1, 'name' => 'Yusuf Bactiar', 'mata_kuliah' => 'pemograman web',
                'mahasiswa' =>
                [
                    ['nama' => 'Muhammad Soleh', 'nilai' => 78],
                    ['nama' => 'Jujun Junaedi', 'nilai' => 85],
                    ['nama' => 'Mamat Alkatiri', 'nilai' => 90],
                ],
            ],
            ['id' => 2, 'name' => 'Yaris Riyadi', 'mata_kuliah' => 'pemograman web',
                'mahasiswa' =>
                [
                    ['nama' => 'Alfred mcTominay', 'nilai' => 67],
                    ['nama' => 'Bruno Kamsir', 'nilai' => 90],
                ],
            ],
        ];
        return view('pages.dosen', ['dosen' => $a]);
    }
    public function televisi()
    {
        $tv = [
            ['jadwal_tv' => 'NET TV',
                'jadwal' => [
                    ['nama_acara' => 'INI TALK SHOW', 'jam_tayang' => '21.00 WIB'],
                ],
            ],
            ['jadwal_tv' => 'TVRI',
                'jadwal' => [
                    ['nama_acara' => 'KAJIAN SORE', 'jam_tayang' => '16.00 WIB'],
                ],
            ],
            ['jadwal_tv' => 'ANTV',
                'jadwal' => [
                    ['nama_acara' => 'Balika Vadhu', 'jam_tayang' => '15:30 WIB'],
                ],
            ],
            ['jadwal_tv' => 'SCTV',
                'jadwal' => [
                    ['nama_acara' => 'Cinta Setelah Cinta', 'jam_tayang' => '19.15 WIB'],
                ],
            ],
        ];
        return view('pages.televisi', ['televisi' => $tv]);
    }
    public function toko()
    {
        $a =
            [
            ['nama_orang' => 'ALFIAN',
                'pembelian' => [
                    ['nama_barang' => 'SEPATU', 'merk' => 'VANS', 'harga_barang' => 250000],
                    ['nama_barang' => 'CELANA', 'merk' => 'CARDINAL', 'harga_barang' => 150000],
                    ['nama_barang' => 'KUPLUK', 'merk' => 'EIGER', 'harga_barang' => 100000],
                    ['nama_barang' => 'BAJU', 'merk' => 'BAONG', 'harga_barang' => 100000],
                ],
            ],
            ['nama_orang' => 'DIDA',
                'pembelian' => [
                    ['nama_barang' => 'TOPI', 'merk' => 'VANS', 'harga_barang' => 100000],
                    ['nama_barang' => 'CELANA', 'merk' => 'H&M', 'harga_barang' => 75000],
                    ['nama_barang' => 'BAJU', 'merk' => '3SECOND', 'harga_barang' => 125000],
                ],
            ],
        ];
        return view('pages.toko', ['toko' => $a]);
    }
    public function siswa_kuliah()
    {
        $mahasiswa =
            [
            ['nama_siswa' => 'agus', 'nama_jurusan' => 'TKR',
                'nilai' => [
                    ['matpel' => 'B.Indonesia', 'nilai' => 80],
                    ['matpel' => 'B.Inggris', 'nilai' => 97],
                    ['matpel' => 'produktif k', 'nilai' => 67],
                    ['matpel' => 'matematika', 'nilai' => 100],
                ],
            ],
            ['nama_siswa' => 'mahmud', 'nama_jurusan' => 'TKR',
                'nilai' => [
                    ['matpel' => 'B.Indonesia', 'nilai' => 78],
                    ['matpel' => 'B.Inggris', 'nilai' => 86],
                    ['matpel' => 'produktif k', 'nilai' => 90],
                    ['matpel' => 'matematika', 'nilai' => 67],
                ],
            ],
            ['nama_siswa' => 'rendi', 'nama_jurusan' => 'TSM',
                'nilai' => [
                    ['matpel' => 'B.Indonesia', 'nilai' => 90],
                    ['matpel' => 'B.Inggris', 'nilai' => 50],
                    ['matpel' => 'produktif k', 'nilai' => 65],
                    ['matpel' => 'matematika', 'nilai' => 78],
                ],
            ],
            ['nama_siswa' => 'firman', 'nama_jurusan' => 'RPL',
                'nilai' => [
                    ['matpel' => 'B.Indonesia', 'nilai' => 78],
                    ['matpel' => 'B.Inggris', 'nilai' => 90],
                    ['matpel' => 'produktif k', 'nilai' => 56],
                    ['matpel' => 'matematika', 'nilai' => 78],
                ],
            ],
            ['nama_siswa' => 'abdul', 'nama_jurusan' => 'RPL',
                'nilai' => [
                    ['matpel' => 'B.Indonesia', 'nilai' => 89],
                    ['matpel' => 'B.Inggris', 'nilai' => 67],
                    ['matpel' => 'produktif k', 'nilai' => 80],
                    ['matpel' => 'matematika', 'nilai' => 90],
                ],
            ],
        ];
        return view('pages.siswa_kuliah', ['siswa_kuliah' => $mahasiswa]);

    }
}
