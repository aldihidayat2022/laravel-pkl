<?php

namespace App\Http\Controllers;

use App\Models\Post;

class PostController extends Controller
{
    public function tampil()
    {
        $post = Post::all();
        return view('post.index', compact('post'));
    }

    public function search_tittle($tittle)
    {
        // MENCARI DATA DARI MODEL POST BERDASARKAN ID
        $post = Post::where('tittle', 'like', '%' . $tittle . '%')->get();
        return $post;
    }

    public function edit($id, $tittle, $content)
    {
        $post = Post::find($id);
        $post->tittle = $id;
        $post->content = $tittle;
        $post->save();
        return $post;
    }
    public function tambah($a, $b)
    {
        $post = new post();
        $post->tittle = $a;
        $post->content = $b;
        $post->save();
        return $post;
    }
    public function hapus($id)
    {
        $post = Post::find($id);
        $post->delete();

        return $post;
    }
}
